package com.example.knnsbp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_sesak.*

class Sesak : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sesak)

        var nyeriPerut = intent.getStringExtra("nyeri perut")
        var skalaNyeriPerut = intent.getIntExtra("skala nyeri perut",0)
        var mual = intent.getStringExtra("mual")
        var muntah = intent.getStringExtra("muntah")
        var nafsuMakan = intent.getStringExtra("nafsuMakan")
        var bab = intent.getStringExtra("BAB")
        var panas = intent.getStringExtra("panas")
        var batuk = intent.getStringExtra("batuk")

        BtnYaSesak.setOnClickListener{
            val intent = Intent(this, NyeriTekan::class.java)
            intent.putExtra("nyeri perut", nyeriPerut)
            intent.putExtra("skala nyeri perut", skalaNyeriPerut)
            intent.putExtra("mual", mual)
            intent.putExtra("muntah", muntah)
            intent.putExtra("nafsuMakan", nafsuMakan)
            intent.putExtra("panas", panas)
            intent.putExtra("BAB", bab)
            intent.putExtra("batuk", batuk)
            intent.putExtra("sesak", "Ya")
            startActivity(intent)
        }

        BtnTidakSesak.setOnClickListener{
            val intent = Intent(this, NyeriTekan::class.java)
            intent.putExtra("nyeri perut", nyeriPerut)
            intent.putExtra("skala nyeri perut", skalaNyeriPerut)
            intent.putExtra("mual", mual)
            intent.putExtra("muntah", muntah)
            intent.putExtra("nafsuMakan", nafsuMakan)
            intent.putExtra("panas", panas)
            intent.putExtra("BAB", bab)
            intent.putExtra("batuk", batuk)
            intent.putExtra("sesak", "Tidak")
            startActivity(intent)
        }
    }
}
