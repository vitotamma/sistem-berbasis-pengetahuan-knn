package com.example.knnsbp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ListView
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_main.*
import java.util.ArrayList




class MainActivity : AppCompatActivity() {

    var skala = 0
    lateinit var ref : DatabaseReference
    lateinit var daftarDataDiagnosa : MutableList<DataDiagnosa>
    var banyakData : Int = 0
    var hasilDiagnosa:String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        ref = FirebaseDatabase.getInstance().getReference("Diagnosa")

        daftarDataDiagnosa = mutableListOf()

        //listView = findViewById(R.id.listView)

        var skalaNyeriPerut = ""
        skalaNyeriPerut = intent.getIntExtra("skala nyeri perut", 0).toString()

        TVNyeriPerut.text = intent.getStringExtra("nyeri perut")
        TVSkalaNyeriPerut.text = skalaNyeriPerut
        TVMual.text = intent.getStringExtra("mual")
        TVMuntah.text = intent.getStringExtra("muntah")
        TVNafsuMakan.text = intent.getStringExtra("nafsuMakan")
        TVBAB.text = intent.getStringExtra("BAB")
        TVPanas.text = intent.getStringExtra("panas")
        TVBatuk.text = intent.getStringExtra("batuk")
        TVSesak.text = intent.getStringExtra("sesak")
        TVNyeriTekan.text = intent.getStringExtra("nyeri tekan")
        TVLeukosit.text = intent.getStringExtra("leukosit")

        skala = skalaNyeriPerut.toInt()

        BtnDiagnosa.setOnClickListener {
            progressBar.setVisibility(View.VISIBLE)
            ref.addListenerForSingleValueEvent(object : ValueEventListener{
                override fun onCancelled(p0: DatabaseError) {
                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }
                override fun onDataChange(p0: DataSnapshot) {
                    if (p0!!.exists()){
                        var jarak : Double = 0.0
//                        var idYangDipilih : Int = 0
//                        var hasilDiagnosa:String = ""
                        var dataKe : Int = 0
                        for (i in p0.children){
                            val data = i.getValue(DataDiagnosa::class.java)
                            daftarDataDiagnosa.add(data!!)
                            var hasil = jarak(kedekatanNyeriPerut(TVNyeriPerut.text.toString(), daftarDataDiagnosa[dataKe].nyeriPerut),
                                kedekatanSkala(skala, daftarDataDiagnosa[dataKe].skalaNyeriPerut),
                                kedekatanMual(TVMual.text.toString(), daftarDataDiagnosa[dataKe].mual),
                                kedekatanMuntah(TVMuntah.text.toString(), daftarDataDiagnosa[dataKe].muntah),
                                kedekatanNafsuMakan(TVNafsuMakan.text.toString(), daftarDataDiagnosa[dataKe].nafsuMakan),
                                kedekatanBAB(TVBAB.text.toString(), daftarDataDiagnosa[dataKe].bab),
                                kedekatanPanas(TVPanas.text.toString(), daftarDataDiagnosa[dataKe].panas),
                                kedekatanBatuk(TVBatuk.text.toString(), daftarDataDiagnosa[dataKe].batuk),
                                kedekatanSesak(TVSesak.text.toString(), daftarDataDiagnosa[dataKe].sesak),
                                kedekatanNyeriTekan(TVNyeriTekan.text.toString(), daftarDataDiagnosa[dataKe].nyeriTekan),
                                kedekatanLeukosit(TVLeukosit.text.toString(), daftarDataDiagnosa[dataKe].leukosit))
                            if (hasil > jarak){
//                                idYangDipilih = daftarDataDiagnosa[dataKe].id
                                jarak = hasil
                                hasilDiagnosa = daftarDataDiagnosa[dataKe].diagnosa
                                getHasilDiagnosa(hasilDiagnosa)
                            }
                            TVHasilDiagnosa.text = hasilDiagnosa
                            dataKe++
                            banyakData = daftarDataDiagnosa.size
                        }
                        //if(jarak <= 1){
                            saveData(hasilDiagnosa)
                        Log.i("jarak ", ""+jarak)
                        //}
                    }
                }
            })
            progressBar.setVisibility(View.GONE)
        }
    }

    fun getHasilDiagnosa(hasil:String){
        hasilDiagnosa = hasil
    }

    fun saveData(hasilDiagnosa:String){
        val dataDiagnosaId: String = ref.push().key.toString()

        val diagnosa = DataDiagnosa(banyakData+1, TVNyeriPerut.text.toString(), skala, TVMual.text.toString(), TVMuntah.text.toString(),
            TVNafsuMakan.text.toString(), TVBAB.text.toString(), TVPanas.text.toString(), TVBatuk.text.toString(), TVSesak.text.toString(),
            TVNyeriTekan.text.toString(), TVLeukosit.text.toString(), hasilDiagnosa)
        ref.child(dataDiagnosaId).setValue(diagnosa)
    }

    fun kedekatanNyeriPerut (inputNyeriPerut : String, kasusNyeriPerut :String) : Double{
        var kedekatanNyeriPerut = 0.0

        if (inputNyeriPerut.equals("kanan bawah") && kasusNyeriPerut.equals("kanan bawah")) {
            kedekatanNyeriPerut = 1.0
        } else if (inputNyeriPerut.equals("Perut Bagian Lain") && kasusNyeriPerut.equals("kanan bawah")) {
            kedekatanNyeriPerut = 0.5
        } else if (inputNyeriPerut.equals("Tidak Nyeri") && kasusNyeriPerut.equals("kanan bawah")) {
            kedekatanNyeriPerut = 0.0
        }

        else if (inputNyeriPerut.equals("kanan bawah") && kasusNyeriPerut.equals("Perut Bagian Lain")) {
            kedekatanNyeriPerut = 0.5
        } else if (inputNyeriPerut.equals("Perut Bagian Lain") && kasusNyeriPerut.equals("Perut Bagian Lain")) {
            kedekatanNyeriPerut = 1.0
        } else if (inputNyeriPerut.equals("Tidak Nyeri") && kasusNyeriPerut.equals("Perut Bagian Lain")) {
            kedekatanNyeriPerut = 0.0
        }

        else if (inputNyeriPerut.equals("kanan bawah") && kasusNyeriPerut.equals("Tidak Nyeri")) {
            kedekatanNyeriPerut = 0.0
        } else if (inputNyeriPerut.equals("Perut Bagian Lain") && kasusNyeriPerut.equals("Tidak Nyeri")) {
            kedekatanNyeriPerut = 0.5
        } else if (inputNyeriPerut.equals("Tidak Nyeri") && kasusNyeriPerut.equals("Tidak Nyeri")) {
            kedekatanNyeriPerut = 1.0
        }

        return kedekatanNyeriPerut;
    }

    fun kedekatanSkala(inputSkala: Int, kasusSkala: Int): Double {
        var kedekatanSkala = 0.0
        if (inputSkala - kasusSkala == Math.abs(0)) {
            kedekatanSkala = 1.0
        } else if (inputSkala - kasusSkala == Math.abs(1)) {
            kedekatanSkala = 0.9
        } else if (inputSkala - kasusSkala == Math.abs(2)) {
            kedekatanSkala = 0.8
        } else if (inputSkala - kasusSkala == Math.abs(3)) {
            kedekatanSkala = 0.7
        } else if (inputSkala - kasusSkala == Math.abs(4)) {
            kedekatanSkala = 0.6
        } else if (inputSkala - kasusSkala == Math.abs(5)) {
            kedekatanSkala = 0.5
        } else if (inputSkala - kasusSkala == Math.abs(6)) {
            kedekatanSkala = 0.4
        } else if (inputSkala - kasusSkala == Math.abs(7)) {
            kedekatanSkala = 0.3
        } else if (inputSkala - kasusSkala == Math.abs(8)) {
            kedekatanSkala = 0.2
        } else if (inputSkala - kasusSkala == Math.abs(9)) {
            kedekatanSkala = 0.1
        } else if (inputSkala - kasusSkala == Math.abs(10)) {
            kedekatanSkala = 0.0
        }
        return kedekatanSkala
    }

    fun kedekatanMual(inputMual: String, kasusMual: String): Double {
        var kedekatanMual = 0.0
        if (inputMual == kasusMual) {
            kedekatanMual = 1.0
        } else
            kedekatanMual = 0.0

        return kedekatanMual
    }

    fun kedekatanMuntah(inputMuntah: String, kasusMuntah: String): Double {
        var kedekatanMuntah = 0.0
        if (inputMuntah == kasusMuntah) {
            kedekatanMuntah = 1.0
        } else
            kedekatanMuntah = 0.0

        return kedekatanMuntah
    }

    fun kedekatanNafsuMakan(inputNafsuMakan: String, kasusNafsuMakan: String): Double {
        var kedekatanNafsuMakan = 0.0
        if (inputNafsuMakan == kasusNafsuMakan) {
            kedekatanNafsuMakan = 1.0
        } else
            kedekatanNafsuMakan = 0.0

        return kedekatanNafsuMakan
    }

    fun kedekatanBAB(inputBAB: String, kasusBAB: String): Double {
        var kedekatanBAB = 0.0
        if (inputBAB == kasusBAB) {
            kedekatanBAB = 1.0
        } else
            kedekatanBAB = 0.0

        return kedekatanBAB
    }

    fun kedekatanPanas(inputPanas: String, kasusPanas: String): Double {
        var kedekatanPanas = 0.0
        if (inputPanas == kasusPanas) {
            kedekatanPanas = 1.0
        } else
            kedekatanPanas = 0.0

        return kedekatanPanas
    }

    fun kedekatanBatuk(inputBatuk: String, kasusBatuk: String): Double {
        var kedekatanBatuk = 0.0
        if (inputBatuk == kasusBatuk) {
            kedekatanBatuk = 1.0
        } else
            kedekatanBatuk = 0.0

        return kedekatanBatuk
    }

    fun kedekatanSesak(inputSesak: String, kasusSesak: String): Double {
        var kedekatanSesak = 0.0
        if (inputSesak == kasusSesak) {
            kedekatanSesak = 1.0
        } else
            kedekatanSesak = 0.0

        return kedekatanSesak
    }

    fun kedekatanNyeriTekan(inputNyeriTekan: String, kasusNyeriTekan: String): Double {
        var kedekatanNyeriTekan = 0.0
        if (inputNyeriTekan == kasusNyeriTekan) {
            kedekatanNyeriTekan = 1.0
        } else
            kedekatanNyeriTekan = 0.0

        return kedekatanNyeriTekan
    }

    fun kedekatanLeukosit(inputLeukosit: String, kasusLeukosit: String): Double {
        var kedekatanLeukosit = 0.0
        if (inputLeukosit == kasusLeukosit) {
            kedekatanLeukosit = 1.0
        } else
            kedekatanLeukosit = 0.0

        return kedekatanLeukosit
    }

    fun jarak(kedekatanNyeriPerut: Double, kedekatanSkala: Double, kedekatanMual: Double,
        kedekatanMuntah: Double, kedekatanNafsuMakan: Double, kedekatanBAB: Double,
        kedekatanPanas: Double, kedekatanBatuk: Double, kedekatanSesak: Double,
        kedekatanNyeriTekan: Double, kedekatanLeukosit: Double): Double {

        var jarak = (kedekatanNyeriPerut * 1 + kedekatanSkala * 0.8 + kedekatanMual * 0.6 + kedekatanMuntah * 0.6
                + kedekatanNafsuMakan * 0.5 + kedekatanBAB * 0.4 + kedekatanPanas * 0.6 + kedekatanBatuk * 0.4
                + kedekatanSesak * 0.4 + kedekatanNyeriTekan * 0.6 + kedekatanLeukosit * 0.8) / 6.7;

        return jarak;
    }

}
