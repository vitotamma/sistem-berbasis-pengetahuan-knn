package com.example.knnsbp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_muntah.*

class Muntah : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_muntah)

        var nyeriPerut = intent.getStringExtra("nyeri perut")
        var skalaNyeriPerut = intent.getIntExtra("skala nyeri perut",0)
        var mual = intent.getStringExtra("mual")

        BtnYaMuntah.setOnClickListener{
            val intent = Intent(this, NafsuMakan::class.java)
            intent.putExtra("nyeri perut", nyeriPerut)
            intent.putExtra("skala nyeri perut", skalaNyeriPerut)
            intent.putExtra("mual", mual)
            intent.putExtra("muntah", "Ya")
            startActivity(intent)
        }

        BtnTidakMuntah.setOnClickListener{
            val intent = Intent(this, NafsuMakan::class.java)
            intent.putExtra("nyeri perut", nyeriPerut)
            intent.putExtra("skala nyeri perut", skalaNyeriPerut)
            intent.putExtra("mual", mual)
            intent.putExtra("muntah", "Tidak")
            startActivity(intent)
        }
    }
}
