package com.example.knnsbp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_mual.*

class Mual : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mual)

        var nyeriPerut = intent.getStringExtra("nyeri perut")
        var skalaNyeriPerut = intent.getIntExtra("skala nyeri perut",0)

        BtnYaMual.setOnClickListener{
            val intent = Intent(this, Muntah::class.java)
            intent.putExtra("nyeri perut", nyeriPerut)
            intent.putExtra("skala nyeri perut", skalaNyeriPerut)
            intent.putExtra("mual", "Ya")
            startActivity(intent)
        }

        BtnTidakMual.setOnClickListener{
            val intent = Intent(this, Muntah::class.java)
            intent.putExtra("nyeri perut", nyeriPerut)
            intent.putExtra("skala nyeri perut", skalaNyeriPerut)
            intent.putExtra("mual", "Tidak")
            startActivity(intent)
        }
    }
}
