package com.example.knnsbp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_nafsu_makan.*

class NafsuMakan : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nafsu_makan)

        var nyeriPerut = intent.getStringExtra("nyeri perut")
        var skalaNyeriPerut = intent.getIntExtra("skala nyeri perut",0)
        var mual = intent.getStringExtra("mual")
        var muntah = intent.getStringExtra("muntah")

        BtnNafsuNormal.setOnClickListener{
            val intent = Intent(this, BAB::class.java)
            intent.putExtra("nyeri perut", nyeriPerut)
            intent.putExtra("skala nyeri perut", skalaNyeriPerut)
            intent.putExtra("mual", mual)
            intent.putExtra("muntah", muntah)
            intent.putExtra("nafsuMakan", "Normal")
            startActivity(intent)
        }

        BtnNafsuMenurun.setOnClickListener{
            val intent = Intent(this, BAB::class.java)
            intent.putExtra("nyeri perut", nyeriPerut)
            intent.putExtra("skala nyeri perut", skalaNyeriPerut)
            intent.putExtra("mual", mual)
            intent.putExtra("muntah", muntah)
            intent.putExtra("nafsuMakan", "Menurun")
            startActivity(intent)
        }
    }
}
