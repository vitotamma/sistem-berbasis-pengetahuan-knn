package com.example.knnsbp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_bab.*
import kotlinx.android.synthetic.main.activity_panas.*

class BAB : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bab)

        var nyeriPerut = intent.getStringExtra("nyeri perut")
        var skalaNyeriPerut = intent.getIntExtra("skala nyeri perut",0)
        var mual = intent.getStringExtra("mual")
        var muntah = intent.getStringExtra("muntah")
        var nafsuMakan = intent.getStringExtra("nafsuMakan")

        BtnMencret.setOnClickListener{
            val intent = Intent(this, Panas::class.java)
            intent.putExtra("nyeri perut", nyeriPerut)
            intent.putExtra("skala nyeri perut", skalaNyeriPerut)
            intent.putExtra("mual", mual)
            intent.putExtra("muntah", muntah)
            intent.putExtra("nafsuMakan", nafsuMakan)
            intent.putExtra("BAB", "Mencret")
            startActivity(intent)
        }

        BtnSembelit.setOnClickListener{
            val intent = Intent(this, Panas::class.java)
            intent.putExtra("nyeri perut", nyeriPerut)
            intent.putExtra("skala nyeri perut", skalaNyeriPerut)
            intent.putExtra("mual", mual)
            intent.putExtra("muntah", muntah)
            intent.putExtra("nafsuMakan", nafsuMakan)
            intent.putExtra("BAB", "Sembelit")
            startActivity(intent)
        }

        BtnNormal.setOnClickListener{
            val intent = Intent(this, Panas::class.java)
            intent.putExtra("nyeri perut", nyeriPerut)
            intent.putExtra("skala nyeri perut", skalaNyeriPerut)
            intent.putExtra("mual", mual)
            intent.putExtra("muntah", muntah)
            intent.putExtra("nafsuMakan", nafsuMakan)
            intent.putExtra("BAB", "Normal")
            startActivity(intent)
        }
    }
}
