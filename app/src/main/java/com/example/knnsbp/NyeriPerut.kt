package com.example.knnsbp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_nyeri_perut.*

class NyeriPerut : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nyeri_perut)

        BtnPerutKananBawah.setOnClickListener{
            val intent = Intent(this, SkalaNyeriPerut::class.java)
            intent.putExtra("nyeri perut", "Kanan Bawah")
            startActivity(intent)
        }

        BtnPerutBagianLain.setOnClickListener {
            val intent = Intent(this, SkalaNyeriPerut::class.java)
            intent.putExtra("nyeri perut", "Perut Bagian Lain")
            startActivity(intent)
        }

        BtnTidakNyeri.setOnClickListener {
            val intent = Intent(this, SkalaNyeriPerut::class.java)
            intent.putExtra("nyeri perut", "Tidak Nyeri")
            startActivity(intent)
        }
    }
}
