package com.example.knnsbp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_panas.*

class Panas : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_panas)

        var nyeriPerut = intent.getStringExtra("nyeri perut")
        var skalaNyeriPerut = intent.getIntExtra("skala nyeri perut",0)
        var mual = intent.getStringExtra("mual")
        var muntah = intent.getStringExtra("muntah")
        var nafsuMakan = intent.getStringExtra("nafsuMakan")
        var bab = intent.getStringExtra("BAB")

        BtnYaPanas.setOnClickListener{
            val intent = Intent(this, Batuk::class.java)
            intent.putExtra("nyeri perut", nyeriPerut)
            intent.putExtra("skala nyeri perut", skalaNyeriPerut)
            intent.putExtra("mual", mual)
            intent.putExtra("muntah", muntah)
            intent.putExtra("nafsuMakan", nafsuMakan)
            intent.putExtra("BAB", bab)
            intent.putExtra("panas", "Ya")
            startActivity(intent)
        }

        BtnTidakPanas.setOnClickListener{
            val intent = Intent(this, Batuk::class.java)
            intent.putExtra("nyeri perut", nyeriPerut)
            intent.putExtra("skala nyeri perut", skalaNyeriPerut)
            intent.putExtra("mual", mual)
            intent.putExtra("muntah", muntah)
            intent.putExtra("nafsuMakan", nafsuMakan)
            intent.putExtra("BAB", bab)
            intent.putExtra("panas", "Tidak")
            startActivity(intent)
        }
    }
}
