package com.example.knnsbp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.SeekBar
import kotlinx.android.synthetic.main.activity_skala_nyeri_perut.*

class SkalaNyeriPerut : AppCompatActivity() {

    var skalaNyeriPerut : Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_skala_nyeri_perut)

        var nyeriPerut = intent.getStringExtra("nyeri perut")

        SBSkalaNyeriPerut.max = 10

        SBSkalaNyeriPerut.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                TVSkalaNyeriPerut.text = seekBar.progress.toString()
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {
                TVSkalaNyeriPerut.text = seekBar.progress.toString()
                skalaNyeriPerut = TVSkalaNyeriPerut.text.toString().toInt()
            }

        })

        BtnNext.setOnClickListener {

            val intent = Intent(this, Mual::class.java)
            intent.putExtra("nyeri perut", nyeriPerut)
            intent.putExtra("skala nyeri perut", skalaNyeriPerut)
            startActivity(intent)
        }
    }
}
