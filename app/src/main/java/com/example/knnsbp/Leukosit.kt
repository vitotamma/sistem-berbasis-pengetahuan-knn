package com.example.knnsbp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_leukosit.*

class Leukosit : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_leukosit)

        var nyeriPerut = intent.getStringExtra("nyeri perut")
        var skalaNyeriPerut = intent.getIntExtra("skala nyeri perut",0)
        var mual = intent.getStringExtra("mual")
        var muntah = intent.getStringExtra("muntah")
        var nafsuMakan = intent.getStringExtra("nafsuMakan")
        var bab = intent.getStringExtra("BAB")
        var panas = intent.getStringExtra("panas")
        var batuk = intent.getStringExtra("batuk")
        var sesak = intent.getStringExtra("sesak")
        var nyeriTekan = intent.getStringExtra("nyeri tekan")

        BtnNormal.setOnClickListener{
            val intent = Intent(this, MainActivity::class.java)
            intent.putExtra("nyeri perut", nyeriPerut)
            intent.putExtra("skala nyeri perut", skalaNyeriPerut)
            intent.putExtra("mual", mual)
            intent.putExtra("muntah", muntah)
            intent.putExtra("nafsuMakan", nafsuMakan)
            intent.putExtra("panas", panas)
            intent.putExtra("BAB", bab)
            intent.putExtra("batuk", batuk)
            intent.putExtra("sesak", sesak)
            intent.putExtra("nyeri tekan", nyeriTekan)
            intent.putExtra("leukosit", "Normal")
            startActivity(intent)
        }

        BtnAbnormal.setOnClickListener{
            val intent = Intent(this, MainActivity::class.java)
            intent.putExtra("nyeri perut", nyeriPerut)
            intent.putExtra("skala nyeri perut", skalaNyeriPerut)
            intent.putExtra("mual", mual)
            intent.putExtra("muntah", muntah)
            intent.putExtra("nafsuMakan", nafsuMakan)
            intent.putExtra("panas", panas)
            intent.putExtra("BAB", bab)
            intent.putExtra("batuk", batuk)
            intent.putExtra("sesak", sesak)
            intent.putExtra("nyeri tekan", nyeriTekan)
            intent.putExtra("leukosit", "Abnormal")
            startActivity(intent)
        }
    }
}
